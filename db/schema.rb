# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_14_185309) do

  create_table "buyers", id: false, force: :cascade do |t|
    t.integer "id"
    t.string "nickname"
    t.string "email"
    t.string "phone"
    t.string "first_name"
    t.string "last_name"
    t.string "doc_type"
    t.string "doc_number"
  end

  create_table "items", id: false, force: :cascade do |t|
    t.string "id"
    t.string "title"
  end

  create_table "order_items", id: false, force: :cascade do |t|
    t.integer "order_id", null: false
    t.integer "item_id", null: false
    t.integer "quantity"
    t.decimal "unit_price"
    t.decimal "full_unit_price"
    t.index ["item_id"], name: "index_order_items_on_item_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
  end

  create_table "orders", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "store_id", null: false
    t.integer "shipping_id", null: false
    t.integer "buyer_id", null: false
    t.datetime "date_created"
    t.datetime "date_closed"
    t.datetime "last_updated"
    t.decimal "total_amount"
    t.decimal "total_shipping"
    t.decimal "total_amount_with_shipping"
    t.decimal "paid_amount"
    t.datetime "expiration_date"
    t.string "status"
    t.index ["buyer_id"], name: "index_orders_on_buyer_id"
    t.index ["shipping_id"], name: "index_orders_on_shipping_id"
    t.index ["store_id"], name: "index_orders_on_store_id"
  end

  create_table "payers", id: false, force: :cascade do |t|
    t.integer "id"
  end

  create_table "payments", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "order_id", null: false
    t.integer "payer_id", null: false
    t.integer "installments"
    t.string "payment_type"
    t.string "status"
    t.decimal "transaction_amount"
    t.decimal "taxes_amount"
    t.decimal "shipping_cost"
    t.decimal "total_paid_amount"
    t.decimal "installment_amount"
    t.datetime "date_approved"
    t.datetime "date_created"
    t.index ["order_id"], name: "index_payments_on_order_id"
    t.index ["payer_id"], name: "index_payments_on_payer_id"
  end

  create_table "receiver_addresses", id: false, force: :cascade do |t|
    t.integer "id"
    t.string "address_line"
    t.string "street_name"
    t.string "street_number"
    t.string "comment"
    t.string "zip_code"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "neighborhood"
  end

  create_table "shippings", id: false, force: :cascade do |t|
    t.integer "id"
    t.string "shipment_type"
    t.datetime "date_created"
    t.integer "receiver_address_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["receiver_address_id"], name: "index_shippings_on_receiver_address_id"
  end

  create_table "stores", id: false, force: :cascade do |t|
    t.integer "id"
  end

  add_foreign_key "order_items", "items"
  add_foreign_key "order_items", "orders"
  add_foreign_key "orders", "buyers"
  add_foreign_key "orders", "shippings"
  add_foreign_key "orders", "stores"
  add_foreign_key "payments", "orders"
  add_foreign_key "payments", "payers"
  add_foreign_key "shippings", "receiver_addresses"
end
