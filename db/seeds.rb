Rails.logger = Logger.new(STDOUT)

order_data = File.read Rails.root.join('config', 'order_sample.json')
parsed_order_data = JSON.parse order_data

parsed_order_data['order_items'].each do |ordem_item|
  Rails.logger.info("Creating Item with: #{ordem_item['item']}")
  Item.create!(ordem_item['item'])
end

Rails.logger.info("Creating a Store with: { id: #{parsed_order_data['store_id']} }")
Store.create!(id: parsed_order_data['store_id'])

buyer_data = {
  id: parsed_order_data['buyer']['id'],
  nickname: parsed_order_data['buyer']['nickname'],
  email: parsed_order_data['buyer']['email'],
  phone: "(#{parsed_order_data['buyer']['phone']['area_code']}) #{parsed_order_data['buyer']['phone']['number']}",
  first_name: parsed_order_data['buyer']['first_name'],
  last_name: parsed_order_data['buyer']['last_name'],
  doc_type: parsed_order_data['buyer']['billing_info']['doc_type'],
  doc_number: parsed_order_data['buyer']['billing_info']['doc_number']
}
Rails.logger.info("Creating a Buyer with: #{buyer_data}")
Buyer.create!(buyer_data)