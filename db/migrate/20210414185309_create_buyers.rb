class CreateBuyers < ActiveRecord::Migration[6.1]
  def change
    create_table :buyers, id: false do |t|
      t.integer :id
      t.string :nickname
      t.string :email
      t.string :phone
      t.string :first_name
      t.string :last_name
      t.string :doc_type
      t.string :doc_number
    end
  end
end
