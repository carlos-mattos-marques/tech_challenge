class CreateReceiverAddresses < ActiveRecord::Migration[6.1]
  def change
    create_table :receiver_addresses, id: false do |t|
      t.integer :id
      t.string :address_line
      t.string :street_name
      t.string :street_number
      t.string :comment
      t.string :zip_code
      t.string :city
      t.string :state
      t.string :country
      t.string :neighborhood
    end
  end
end
