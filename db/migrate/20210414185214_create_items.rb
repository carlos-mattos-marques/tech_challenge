class CreateItems < ActiveRecord::Migration[6.1]
  def change
    create_table :items, id: false do |t|
      t.string :id
      t.string :title
    end
  end
end
