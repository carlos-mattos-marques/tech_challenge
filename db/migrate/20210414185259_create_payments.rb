class CreatePayments < ActiveRecord::Migration[6.1]
  def change
    create_table :payments, id: false do |t|
      t.integer :id
      t.references :order, null: false, foreign_key: true
      t.references :payer, null: false, foreign_key: true
      t.integer :installments
      t.string :payment_type
      t.string :status
      t.decimal :transaction_amount
      t.decimal :taxes_amount
      t.decimal :shipping_cost
      t.decimal :total_paid_amount
      t.decimal :installment_amount
      t.datetime :date_approved
      t.datetime :date_created
    end
  end
end
