class CreateOrderItems < ActiveRecord::Migration[6.1]
  def change
    create_table :order_items, id: false do |t|
      t.references :order, null: false, foreign_key: true
      t.references :item, null: false, foreign_key: { to_table: :items }
      t.integer :quantity
      t.decimal :unit_price
      t.decimal :full_unit_price
    end
  end
end