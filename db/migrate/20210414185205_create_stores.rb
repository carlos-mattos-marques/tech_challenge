class CreateStores < ActiveRecord::Migration[6.1]
  def change
    create_table :stores, id: false do |t|
      t.integer :id
    end
  end
end
