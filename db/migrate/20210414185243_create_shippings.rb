class CreateShippings < ActiveRecord::Migration[6.1]
  def change
    create_table :shippings, id: false do |t|
      t.integer :id
      t.string :shipment_type
      t.datetime :date_created
      t.references :receiver_address, null: false, foreign_key: true

      t.timestamps
    end
  end
end
