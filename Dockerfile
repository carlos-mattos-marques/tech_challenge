FROM ruby:3.0.1

ARG RUBYGEMS_VERSION="3.2.15"
ARG BUNDLER_VERSION="2.2.15"

ENV BUNDLE_JOBS="4"

RUN apt-get update --quiet=2 && \
    apt-get install --yes --no-install-recommends \
      git \
      cmake \
      pkg-config \
      build-essential && \
    rm --force --recursive /var/lib/apt/lists/*

RUN gem update --system $RUBYGEMS_VERSION && \
    gem uninstall bundler && \
    gem install bundler --version $BUNDLER_VERSION

COPY vendor/cache/*.gem vendor/cache/
COPY Gemfile* ./

FROM bundler AS bundler-development

RUN bundle install

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]
