### Delivery Center Challenge ###

* JSON Parser / API Consumer

### Aaaaand? ###

- [x] Abstract the sample JSON/Request to Models keeping all nice and tidy rails like for proper storage
- [ ] Wrap Delivery's API on a service using Faraday/HTTParty/whatevs
- [ ] Write service that takes the stored Request/JSON from the DB, built a (well a Virtus model would be nice), Uses the API "client" and sends the built payload (POST of course/And yes the client always would set the proper header)
- [ ] Grab response (from POST), check for validation/schema errors, and (by now all of it would be triggered by a request to this app and the response would be a 201 with a status body, the later steps should be async jobs)