class Order < ApplicationRecord
  belongs_to :store
  belongs_to :shipping
  belongs_to :buyer
  
  has_many :payments
  has_many :order_items
end
